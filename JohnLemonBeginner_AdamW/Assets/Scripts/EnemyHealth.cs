﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health = 3;
    AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;
        source.Play();

        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
