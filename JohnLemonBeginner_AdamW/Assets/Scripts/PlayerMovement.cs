﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //set rotation speed
    public float turnSpeed = 30f;

    //References to components
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    public GameObject projectilePrefab;
    public Transform shotSpawn;
    public float shotSpeed = 10f;
    public GameObject globalPost;
    public int count;
    public GameObject[] allKeys;
   

    // Start is called before the first frame update
    void Start()
    {
        //references to player components
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        count = 0;

       

    }

    public void Update()
    {
        
            if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject Projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation);
            Rigidbody projectileRB = Projectile.GetComponent<Rigidbody>();
            projectileRB.velocity = transform.forward * shotSpeed;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Create a new float variable: Horizontal and Vertical
        float horizontal = Input.GetAxis ("Horizontal");
        float vertical = Input.GetAxis ("Vertical");

        // Set values to movement variable
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        // Identify whether there is player input
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        // Set animator component to iswalking based on player input
        m_Animator.SetBool("IsWalking", isWalking);

        //if the player is walking this calls the footsteps audio to play
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }
        // create a roation of the character
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    //apply movemtent and rotation to character
    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        // ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            globalPost.SetActive(false);
            
        }


        //set it so that once the first key is picked up the 2 and then 3rd will appear
        if(other.gameObject.CompareTag("Key"))
        {
            other.gameObject.SetActive(false);
            
            count++;
            allKeys[count].SetActive(true);

        }
    }

}
